package com.dolphs.suelib.business;

import com.dolphs.html.TableNotFoundException;
import com.dolphs.html.impl.GenericHtmlParser;
import com.dolphs.html.impl.TableExtractor;
import sheets.beans.ISpreadSheet;
import sheets.beans.WorkingBook;
import sheets.ioable.Ioable;
import sheets.ioable.MicrosoftSpreadSheet2017;

import java.io.File;
import java.io.IOException;
import java.text.Normalizer;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class SueliPageExtractor extends TableExtractor<Postagem> {

    public SueliPageExtractor() {
        super(Postagem.class);
    }

    @Override
    public void readByTitle(String title, Postagem object, String value) {
        String normalizedTitle =  Normalizer
                .normalize(title, Normalizer.Form.NFD)
                .replaceAll("[^\\p{ASCII}]", "").toLowerCase();
        switch (normalizedTitle) {

            case "#":
                object.setNumero(Integer.parseInt(value));
                break;

            case "titulo":
                object.setTitulo(value);
                break;

            case "publicado":
                object.setPublicado(value);
                break;

            case "pagina inicial":
                object.setPaginaInicial(value);
                break;

            case "ordem":
                object.setOrdem(value);
                break;

            case "nivel de acesso":
                object.setNivelDeAcesso(value);
                break;

            case "secao":
                object.setSecao(value);
                break;

            case "categoria":
                object.setCategoria(value);
                break;

            case "autor":
                object.setAutor(value);
                break;

            case "data":
                try {
                    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd.MM.yy", Locale.US);
                    Date date = simpleDateFormat.parse(value);
                    object.setData(date);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                break;

            case "acesso":
                object.setAcesso(Integer.parseInt(value));
                break;

            case "id":
                object.setId(Integer.parseInt(value));
                break;
        }

    }
}
