package com.dolphs.suelib.business;

import com.dolphs.html.TableNotFoundException;
import com.dolphs.html.impl.GenericHtmlParser;
import dolphs.io.FileFilterD;
import dolphs.io.FileUtils;
import sheets.beans.ISpreadSheet;
import sheets.beans.WorkingBook;
import sheets.ioable.Ioable;
import sheets.ioable.MicrosoftSpreadSheet2017;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * This class is responsible to find and extract the Values from HTML files
 */
public class Extractor {

    private final static Logger logger = Logger.getLogger(Extractor.class.getSimpleName());

    public Extractor() {
    }

    /**
     * This method finds the HTML files from the InputPath, extracted the values of a specific table and
     * saves it as xls and csv file.
     *
     * @param inputPath, the Input Path containing the HTML files.
     * @param outputPath, the Output Path where the .csv and .xls files will be saved.
     * @throws IOException, If some file exception occur it will be thrown.
     */
    public void extract(String inputPath, String outputPath) throws IOException, TableNotFoundException, InstantiationException
            , IllegalAccessException {
        File inputFile = new File(inputPath);
        File outputFile = new File(outputPath);

        List<Postagem> postagemList = new ArrayList<>();
        List<File> files = FileUtils.list(inputFile, new FileFilterD(false, true, "html"));
        String[] header = null;
        for (File file : files) {
            try {
                GenericHtmlParser genericHtmlParser = new GenericHtmlParser(file, "UTF-8");
                String[][] table = genericHtmlParser.getTableDataByTag("table", 2);
                header = table[0];
                SueliPageExtractor sueliPageExtractor = new SueliPageExtractor();
                postagemList.addAll(sueliPageExtractor.convert(table));
            } catch (Exception e) {
                System.out.println(file.getAbsolutePath());
            }
        }

        WorkingBook workingBook = new WorkingBook();

        createAndPopulateSheet(workingBook, header, postagemList);

        Ioable ioable = new MicrosoftSpreadSheet2017();
        ioable.write(workingBook, outputFile);
    }

    private void createAndPopulateSheet(WorkingBook workingBook, String[] header, List<Postagem> postagemList) {
        ISpreadSheet spreadSheetDP = workingBook.createSheet("Diario de Pernambuco");
        ISpreadSheet spreadSheetFP = workingBook.createSheet("Folha de Pernambuco");
        ISpreadSheet spreadSheetJC = workingBook.createSheet("Jornal do Commercio");

        if (header != null) {
            spreadSheetDP.addRow(new ArrayList<>(Arrays.asList(header)));
            spreadSheetDP.removeColumn(1);
            spreadSheetFP.addRow(new ArrayList<>(Arrays.asList(header)));
            spreadSheetFP.removeColumn(1);
            spreadSheetJC.addRow(new ArrayList<>(Arrays.asList(header)));
            spreadSheetJC.removeColumn(1);
        }

        for (Postagem postagem : postagemList) {
            List<Object> values = Arrays.asList(postagem.getNumero(), postagem.getTitulo(), postagem.getPublicado(), postagem.getPaginaInicial(),
                    postagem.getOrdem(), postagem.getNivelDeAcesso(), postagem.getSecao(), postagem.getCategoria(), postagem.getAutor(), postagem.getData(),
                    postagem.getAcesso(), postagem.getId());
            if (postagem.getSecao() == null) {
                System.out.println(postagem.getId());
            }
            switch (postagem.getSecao()) {
                case "Diario de Pernambuco":
                    spreadSheetDP.addRow(values);
                    break;
                case "Folha de Pernambuco":
                    spreadSheetFP.addRow(values);
                    break;
                case "Jornal do Commercio":
                    spreadSheetJC.addRow(values);
                    break;
            }
        }
    }
}
