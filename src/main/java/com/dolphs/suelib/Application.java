package com.dolphs.suelib;

import com.dolphs.html.TableNotFoundException;
import com.dolphs.suelib.business.Extractor;
import net.sourceforge.argparse4j.ArgumentParsers;
import net.sourceforge.argparse4j.inf.ArgumentParser;
import net.sourceforge.argparse4j.inf.ArgumentParserException;
import net.sourceforge.argparse4j.inf.Namespace;

import java.io.IOException;

public class Application {

    public static void main(String[] args) throws IOException, IllegalAccessException, TableNotFoundException,
            InstantiationException {

        ArgumentParser parser = ArgumentParsers.newFor("Suelib").build()
                .defaultHelp(true)
                .description("Extract relevant content from HTML files.");
        parser.addArgument("-i", "--input").required(true)
                .help("The input HTML file (or fold if the -f flag is true").type(String.class);
        parser.addArgument("-o", "--output").help("The output csv file (or fold if the -f flag is true\")")
                .type(String.class);
        Namespace ns = null;
        try {
            ns = parser.parseArgs(args);
        } catch (ArgumentParserException e) {
            parser.handleError(e);
            System.exit(1);
        }

        String input = ns.getString("input");
        String output = ns.getString("output");

        Extractor extractor = new Extractor();
        extractor.extract(input, output);

    }
}
