package com.dolphs.suelib;

import com.dolphs.suelib.business.Extractor;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.ActionEvent;
import java.io.File;

public class SwingGUI {

    private static String input;
    private static String output;

    public static void main(String[] args) {


        JFrame jFrame = new JFrame("Tabela de Not\u00EDcias para Excel");
        JLabel firstTipLabel = new JLabel("Selecione a pasta contendo todas as p\u00E1ginas da WEB");
        JLabel secondTipLabel = new JLabel("(Di\u00E1rio de Pernambuco, Jornal do Com\u00E9rcio e Folha)");
        JButton selectButton = new JButton("SELECIONAR");
        JLabel pathLabel = new JLabel("Nenhum arquivo selecionado");
        JButton extractButton = new JButton("EXTRAIR");

        firstTipLabel.setBounds(10, 40, 500, 40);
        secondTipLabel.setBounds(10, 60, 500, 40);

        selectButton.setBounds(115, 100, 130, 40);
        pathLabel.setBounds(10, 150, 500, 40);

        extractButton.setBounds(115, 200, 130, 40);
        extractButton.setEnabled(false);

        jFrame.add(firstTipLabel);
        jFrame.add(secondTipLabel);
        jFrame.add(pathLabel);
        jFrame.add(selectButton);
        jFrame.add(extractButton);

        jFrame.setSize(400, 500);
        jFrame.setLayout(null);
        jFrame.setVisible(true);
        jFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        selectButton.addActionListener((ActionEvent e) -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int retorno = fileChooser.showOpenDialog(null);

            if (retorno == JFileChooser.APPROVE_OPTION) {
                File fileChoosen = fileChooser.getSelectedFile();
                input = fileChoosen.getAbsolutePath();
                pathLabel.setText(input);
                extractButton.setEnabled(true);
            } else {
                extractButton.setEnabled(false);
                pathLabel.setText("Nenhum arquivo selecionado");
                input = null;
            }
        });

        extractButton.addActionListener((ActionEvent a) -> {
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setAcceptAllFileFilterUsed(false);
            fileChooser.setFileFilter(new FileNameExtensionFilter("Excel file *.xlsx", "xlsx"));
            int retorno = fileChooser.showSaveDialog(null);

            if (retorno == JFileChooser.APPROVE_OPTION) {

                new Thread(() -> {
                     try {
                        extractButton.setText("SALVANDO...");
                        extractButton.setEnabled(false);


                        File fileChoosen = fileChooser.getSelectedFile();
                        output = fileChoosen.getAbsolutePath();
                        if (!output.endsWith(".xlsx")) {
                            output = output + ".xlsx";
                        }
                        Extractor extractor = new Extractor();
                        extractor.extract(input, output);
                        output = null;
                        JOptionPane.showMessageDialog(jFrame, "Arquivo salvo com sucesso!", "OK",
                                JOptionPane.INFORMATION_MESSAGE);
                        extractButton.setText("EXTRAIR");
                        extractButton.setEnabled(true);

                    } catch (Exception e) {
                        e.printStackTrace();
                        JOptionPane.showMessageDialog(jFrame, e.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                    }
                }).start();
            } else {
                output = null;
                input = null;
                JOptionPane.showMessageDialog(jFrame, "Selecione um arquivo antes.", "Atenção!", JOptionPane.WARNING_MESSAGE);
            }
        });
    }
}
