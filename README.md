#About
This program was developed to help a family member to extract information from a HTML page.
It basically finds a specific page, parses it, finds the values and put it in a .csv and .xls file.

#Build Jar
To build the jar file just use the command: ``gradlew buildJar``.
A jar will be created in ``[ProjectPath]/build/libs/`` path.

# How to use
Go to the path where the jar is. Default: ``[ProjectPath]/build/libs``

Use the given command:
``java jar [JAR_NAME] -i [INPUT_FOLDER] -o [OUTPUT_FOLDER]``

The [JAR_NAME] is the name of generated jar. Defaul: ``SueLib-all-1.0-SNAPSHOT.jar``
The [INPUT_FOLDER] is the path where the HTML files are stored.
The [OUTPUT_FOLDER] is the target folder where the .xls and .csv files will be saved.

Eg.: ``java -jar SueLib-all-1.0-SNAPSHOT.jar -i "D:\Users\rodol\Documents\clipping 2017\diario de pernambuco" -o "D:\Users\rodol\Documents\output\diario de pernambuco" ``.

This command will find all HTML files in "D:\Users\rodol\Documents\clipping 2017\diario de pernambuco" path, gets the tables
and save the values in "D:\Users\rodol\Documents\output\diario de pernambuco" in .csv and .xls format.